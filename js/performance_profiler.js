/**
 * @file
 * The performance_profiler behavior.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.performanceProfiler = {
    attach: function (context, settings) {
      $(once('performance-profiler-short', '.performance-profiler-short div', context)).each(function () {
        $(this).on('click', function () {
          var $wrapper = $(this).closest('div.performance-profiler-short');
          $wrapper.toggleClass('open');
          $wrapper.parent().toggleClass('open');
          $wrapper.next('.performance-profiler-long').toggleClass('hidden');
        });
      });

      $(once('performance-profiler-long', '.performance-profiler-long a.show-details', context)).each(function () {
        $(this).on('click', function () {
          $('div.db-details:not(.hidden)').remove();
          var $dbDetails = $('div.db-details.hidden', $(this).closest('.db-details-wrapper')).clone();
          $dbDetails.removeClass('hidden').addClass('db-details-shown');
          $('body').append($dbDetails);
          var scrollTop = $(document).height() - $dbDetails.innerHeight();
          $('html, body').animate({ scrollTop: scrollTop - 80 }, 800);
          Drupal.attachBehaviors(document.getElementById('db-details'));
          $('#performance-profiler-dynamic').hide();
        });
      });

      $(once('db-details-inner', 'div.db-details:not(.hidden) .db-details-inner a.hide-details', context)).each(function () {
        $(this).on('click', function () {
          $(this).closest('div.db-details').remove();
          $('#performance-profiler-dynamic').show();
        });
      });
    }
  };

})(jQuery, Drupal);
