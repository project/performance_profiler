<?php

/**
 * @file
 * Performance Profiler Views integration.
 */

/**
 * Implements hook_views_data().
 */
function performance_profiler_views_data() {
  $data = [];

  // Base table info.
  $data['performance_profiler_logs']['table'] = [
    'group' => 'Performance Profiler logs',
    'provider' => 'performance_profiler',
    'base' => [
      'title' => t('Performance Profiler logs'),
      'help' => t('Data logged by the Performance Profiler module.'),
    ],
  ];

  // Numeric field type.
  $fields = [
    'id' => 'ID',
    'run_time' => 'Run time',
    'memcache_time' => 'Memcache time',
    'memory_peak' => 'Memory peak',
    'time' => 'Query total time',
    'count' => 'Query total count',
    'avg' => 'Query total average time',
    'read_time' => 'Query read time',
    'read_count' => 'Query read count',
    'read_avg' => 'Query read average time',
    'write_time' => 'Query write time',
    'write_count' => 'Query write count',
    'write_avg' => 'Query write average time',
    'write_p' => 'Query write percentage',
    'write_p_c' => 'Query write percentage count',
  ];

  foreach ($fields as $field => $title) {
    $data['performance_profiler_logs'][$field] = [
      'title' => t($title),
      'help' => t('Data logged by Performance Profiler.'),
      'field' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
    ];
  }

  // String field type.
  $data['performance_profiler_logs']['path'] = [
    'title' => t('Path'),
    'help' => t('Path of the logged data.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  // Timestamp field type.
  $data['performance_profiler_logs']['timestamp'] = [
    'title' => t('Created'),
    'help' => t('The time that the log was created.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  // User field type.
  $data['performance_profiler_logs']['uid'] = [
    'title' => t('User ID'),
    'help' => t('The user associated with the record.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'title' => t('User ID'),
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'relationship' => [
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
      'label' => t('User'),
    ],
  ];

  return $data;
}
