<?php

namespace Drupal\performance_profiler;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Provides a PerformanceLogger service.
 */
class PerformanceLogger {

  /**
   * Table name.
   *
   * @var string
   */
  protected const TABLE = 'performance_profiler_logs';

  /**
   * Maximum allowed HARD limit.
   *
   * @var int
   */
  protected const MAXLIMIT = 999999;

  /**
   * Number of rows to delete in a single query.
   *
   * @var int
   */
  protected const ROWS = 50;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Drupal DateTime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;

  /**
   * Messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class construct.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   State interface service.
   * @param \Drupal\Component\Datetime\TimeInterface $dateTime
   *   The Drupal DateTime service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $database, TimeInterface $dateTime, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->dateTime = $dateTime;
    $this->messenger = $messenger;
  }

  /**
   * Log the query.
   *
   * @param string $data
   *   Data to be inserted in the DB.
   */
  public function logData(array $data): void {
    if ($this->enforceLimit()) {
      $data['timestamp'] = $this->dateTime->getCurrentTime();
      try {
        $this->database->insert(self::TABLE)
          ->fields($data)
          ->execute();
      }
      catch (\Exception $e) {
        $this->messenger->addWarning('Failed to log data, ' . $e->getMessage());
      }
    }
  }

  /**
   * Enforce a limit on the table number of rows.
   *
   * @return bool
   *   True if successfull.
   */
  private function enforceLimit(): bool {
    $config = $this->configFactory->get('performance_profiler.settings');
    // Do not allow a limit greater than 999999.
    $limit = self::MAXLIMIT;
    if ($config->get('limit') && $config->get('limit') <= self::MAXLIMIT) {
      $limit = $config->get('limit');
    }
    // Count table rows and check if limit is reached.
    $count = $this->database->query("SELECT COUNT(*) FROM {" . self::TABLE . "}")->fetchField();
    if ($count - $limit > self::ROWS) {
      try {
        // Select oldest rows.
        $rows = $this->database->select(self::TABLE, 'r')
          ->fields('r', ['id'])
          ->orderBy('id', 'ASC')
          ->range(0, $count - $limit + 1)
          ->execute()
          ->fetchCol();
        // Delete the rows based on auto-incrementing ID.
        if (!empty($rows)) {
          $this->database->delete(self::TABLE)
            ->condition('id', end($rows), '<')
            ->execute();
        }
      }
      catch (\Exception $e) {
        $this->messenger->addWarning('Failed to log data, ' . $e->getMessage());
        return FALSE;
      }
    }
    return TRUE;
  }

}
