<?php

namespace Drupal\performance_profiler\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigInstallerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Drupal\views\Entity\View;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Memory profiler module settings.
 *
 * @internal
 */
class PerformanceProfilerForm extends ConfigFormBase {

  /**
   * View name.
   *
   * @var string
   */
  private const VIEW = 'views.view.performance_profiler_logs';

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder
   */
  protected $routeBuilder;

  /**
   * The config installer.
   *
   * @var \Drupal\Core\Config\ConfigInstallerInterface
   */
  protected $configInstaller;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Routing\RouteBuilder $route_builder
   *   The route builder.
   * @param \Drupal\Core\Config\ConfigInstallerInterface $config_installer
   *   The config installer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, ModuleExtensionList $extension_list_module, RouteBuilder $route_builder, ConfigInstallerInterface $config_installer) {
    parent::__construct($config_factory);
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->extensionListModule = $extension_list_module;
    $this->routeBuilder = $route_builder;
    $this->configInstaller = $config_installer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('extension.list.module'),
      $container->get('router.builder'),
      $container->get('config.installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'performance_profiler_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['performance_profiler.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $site_config = $this->config('performance_profiler.settings');

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance'),
      '#open' => TRUE,
    ];
    $form['appearance']['watchdog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log statistics into watchdog'),
      '#default_value' => $site_config->get('watchdog'),
      '#description' => $this->t('If checked, the message will be logged via watchdog system.'),
    ];
    $form['appearance']['toolbar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log statistics into Toolbar'),
      '#default_value' => $site_config->get('toolbar'),
      '#description' => $this->t('If checked, the message will be printed at the Toolbar header.'),
    ];
    if (!$this->moduleHandler->moduleExists('toolbar')) {
      $form['appearance']['toolbar']['#attributes'] = ['disabled' => 'disabled'];
      $form['appearance']['toolbar']['#default_value'] = 0;
    }
    $form['appearance']['popup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log statistics into Popup'),
      '#default_value' => $site_config->get('popup'),
      '#description' => $this->t('If checked, the message will be printed at the bottom right corner.'),
    ];
    $form['appearance']['views'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log statistics into Views'),
      '#default_value' => $site_config->get('views'),
      '#description' => $this->t('If checked, save logs to database and integrate with views.'),
    ];
    if (!$this->moduleHandler->moduleExists('views')) {
      $form['appearance']['views']['#attributes'] = ['disabled' => 'disabled'];
      $form['appearance']['views']['#default_value'] = 0;
    }
    $form['appearance']['anonymous'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log statistics for anonymous user'),
      '#default_value' => $site_config->get('anonymous'),
      '#description' => $this->t('If checked, each request from anonymous user will be logged.'),
    ];
    $form['entries'] = [
      '#type' => 'details',
      '#title' => $this->t('Log entries'),
      '#open' => TRUE,
    ];
    $form['entries']['database'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log Database queries'),
      '#default_value' => $site_config->get('database'),
      '#description' => $this->t('Log DB queries statistics (count of read and write queries, execution time).'),
    ];
    $form['entries']['self'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log self AJAX query'),
      '#default_value' => $site_config->get('self'),
      '#description' => $this->t('Log query to get data from Performance profiler, usually should be disabled.'),
    ];
    $form['entries']['self_pages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log self pages'),
      '#default_value' => $site_config->get('self_pages'),
      '#description' => $this->t('Log pages of Performance profiler, usually should be disabled.'),
    ];
    $form['entries']['ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log AJAX queries'),
      '#default_value' => $site_config->get('ajax'),
      '#description' => $this->t('Log XHR queries (contextual, overlay, etc).'),
    ];

    $form['track'] = [
      '#type' => 'details',
      '#title' => $this->t('Track'),
      '#open' => TRUE,
    ];
    $form['track']['memory'] = [
      '#type' => 'number',
      '#title' => $this->t('Min memory usage to track'),
      '#default_value' => $site_config->get('memory'),
      '#description' => $this->t('If not empty or more then 0, will be tracked only higher values.'),
      '#field_suffix' => 'Mb',
      '#size' => 10,
      '#min' => 0,
      '#step' => 0.1,
    ];
    $form['track']['time'] = [
      '#type' => 'number',
      '#title' => $this->t('Min execution time to track'),
      '#default_value' => $site_config->get('time'),
      '#description' => $this->t('If not empty or more then 0, will be tracked only higher values (e.g. 4 or 5.5).'),
      '#field_suffix' => 'seconds',
      '#size' => 10,
      '#min' => 0,
      '#step' => 0.1,
    ];
    $form['track']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Max logs to save in database'),
      '#default_value' => $site_config->get('limit'),
      '#description' => $this->t('A limit of 999999 is enforced to prevent excesive database size.'),
      '#field_suffix' => 'logs',
      '#size' => 10,
      '#min' => 1,
      '#step' => 1,
    ];
    if (!$this->moduleHandler->moduleExists('views')) {
      $form['track']['limit']['#attributes'] = ['disabled' => 'disabled'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Manage default logger view.
    $view_processed = FALSE;
    if ($this->moduleHandler->moduleExists('views') &&
      $form['appearance']['views']['#default_value'] != $form_state->getValue('views')) {
      $view_processed = $this->manageLoggerView($form_state->getValue('views'));
    }
    $views = $form_state->getValue('views');
    if (!$view_processed) {
      $views = FALSE;
    }
    // Submit data.
    $this->config('performance_profiler.settings')
      ->set('ajax', $form_state->getValue('ajax'))
      ->set('anonymous', $form_state->getValue('anonymous'))
      ->set('database', $form_state->getValue('database'))
      ->set('views', $views)
      ->set('memory', $form_state->getValue('memory'))
      ->set('popup', $form_state->getValue('popup'))
      ->set('self', $form_state->getValue('self'))
      ->set('self_pages', $form_state->getValue('self_pages'))
      ->set('time', $form_state->getValue('time'))
      ->set('toolbar', $form_state->getValue('toolbar'))
      ->set('watchdog', $form_state->getValue('watchdog'))
      ->set('limit', $form_state->getValue('limit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Create and manage performance_profiler_logs view depending on status.
   *
   * @param bool $status
   *   Status setting for view logging.
   *
   * @return void
   *   Import or change status for view
   */
  private function manageLoggerView(bool $status): bool {
    $view = View::load('performance_profiler_logs');
    if (!$view && $status) {
      $this->configInstaller->installOptionalConfig(NULL, [
        'module' => 'performance_profiler',
      ]);
      $view = View::load('performance_profiler_logs');
      if (!$view) {
        $this->messenger()->addError('Could not initialize "performance_profiler_logs" view, please check dependencies.');
        return FALSE;
      }
    }
    if ($view && $view->get('status') != $status) {
      $view->set('status', $status)
        ->save();

      // Rebuild routes to show the new view tab.
      $this->messenger()->addWarning('If Performance profiler menu tabs not updated, please wait few seconds and reload the page.');
      $this->routeBuilder->setRebuildNeeded();
    }

    return TRUE;
  }

}
